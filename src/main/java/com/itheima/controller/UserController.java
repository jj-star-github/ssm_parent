package com.itheima.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/getInfo")
    public String getInfo(){
        return "hello itheima itcast";
    }

    @GetMapping("/demo")
    public String demo(){
        return "test auto build and deploy";
    }
}
